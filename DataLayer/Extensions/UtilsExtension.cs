﻿using AutoMapper;

namespace DataLayer.Extensions
{
    public static class UtilsExtension
    {
        public static T Mapping<T>(this object src)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap(src.GetType(), typeof (T)));
            return (T) config.CreateMapper().Map(src, src.GetType(), typeof (T));
        }
    }
}
