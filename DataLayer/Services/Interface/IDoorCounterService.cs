﻿using System.Collections.Generic;
using DataLayer.Models.DoorCounter;
using DataLayer.Models.Dor;

namespace DataLayer.Services.Interface
{
    public interface IDoorCounterService
    {
        void AddBulk(IEnumerable<FootTrafficDataModel> models);

        void Add(FootTrafficDataModel model);

        IEnumerable<DoorCounterModel> ConvertToDoorCounterModels(IEnumerable<FootTrafficDataModel> models);

        DoorCounterModel ConvertToDoorCounterModel(FootTrafficDataModel model);
    }
}
