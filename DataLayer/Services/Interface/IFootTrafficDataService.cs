﻿using DataLayer.Models.Dor;

namespace DataLayer.Services.Interface
{
    public interface IFootTrafficDataService
    {
        void Add(FootTrafficDataModel model);
    }
}
