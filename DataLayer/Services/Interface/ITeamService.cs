﻿using DataLayer.Models.Dor;

namespace DataLayer.Services.Interface
{
    public interface ITeamService
    {
        void Add(TeamModel model);
    }
}
