﻿using DataLayer.Models.Dor;

namespace DataLayer.Services.Interface
{
    public interface IStoreService
    {
        void Add(StoreModel model);
    }
}
