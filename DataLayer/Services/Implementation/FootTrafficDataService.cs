﻿using System;
using DataLayer.Models.Dor;
using DataLayer.Repository.Interface;
using DataLayer.Services.Interface;
using NLog;

namespace DataLayer.Services.Implementation
{
    public class FootTrafficDataService : IFootTrafficDataService
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();
        private readonly IFootTrafficDataRepository _footTrafficDataRepo;

        public FootTrafficDataService(IFootTrafficDataRepository footTrafficDataRepo)
        {
            _footTrafficDataRepo = footTrafficDataRepo;
        }

        public void Add(FootTrafficDataModel model)
        {
            try
            {
                _footTrafficDataRepo.Add(model);
            }
            catch (Exception ex)
            {
                Logger.Error("Foot Traffic Data Service | Add | exception message: {0}", ex.Message);
            }
        }
    }
}
