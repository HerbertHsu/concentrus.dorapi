﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DataLayer.Models.DoorCounter;
using DataLayer.Models.Dor;
using DataLayer.Repository.Interface;
using DataLayer.Services.Interface;
using NLog;
using Newtonsoft.Json;

namespace DataLayer.Services.Implementation
{
    public class DoorCounterService : IDoorCounterService
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        private readonly IDoorCounterRepository _doorCounterRepo;
        private readonly ICustomerRepository _customerRepository;

        public DoorCounterService(IDoorCounterRepository doorCounterRepo, ICustomerRepository customerRepository)
        {
            _doorCounterRepo = doorCounterRepo;
            _customerRepository = customerRepository;
        }

        public void AddBulk(IEnumerable<FootTrafficDataModel> models)
        {
            try
            {
                var doorCounterModels = ConvertToDoorCounterModels(models);
                var doorCounterList = doorCounterModels.ToList();

                if (doorCounterList.Count < 24)
                {
                    doorCounterList = AddMissingHours(doorCounterModels);
                }

                _doorCounterRepo.AddBulk(doorCounterList);
            }
            catch (Exception ex)
            {
                Logger.Error("Door Counter Service | AddBulk | exception message: {0}", ex.Message);
            }
        }

        public void Add(FootTrafficDataModel model)
        {
            try
            {
                var customerIsExists = _customerRepository.IsExist(model.Store_Id);

                if (!customerIsExists)
                {
                    Logger.Error("DoorCounterService | This Store Id: {0} doesn't exists on Customer Table", model.Store_Id);
                    return;
                }

                var doorCounterModel = ConvertToDoorCounterModel(model);
                _doorCounterRepo.Add(doorCounterModel);
            }
            catch (Exception ex)
            {
                Logger.Error("Door Counter Service | Add | exception message: {0}", ex.Message);
            }
        }

        public IEnumerable<DoorCounterModel> ConvertToDoorCounterModels(IEnumerable<FootTrafficDataModel> models)
        {
            //just get the Store_Id in any element, since all elements have same Store_Id
            var storeId = models.First().Store_Id;
            var customerModel = _customerRepository.FindByStoreId(storeId);

            var config = new MapperConfiguration(cfg => cfg.CreateMap<FootTrafficDataModel, DoorCounterModel>()
                .ForMember(dest => dest.SiteId, opt => opt.MapFrom(src => src.Store_Id))
                .ForMember(dest => dest.CustomerCID, opt => opt.MapFrom(src => customerModel.CustomerCID ?? string.Empty))
                .ForMember(dest => dest.CustomerName, opt => opt.MapFrom(src => customerModel.CustomerName ?? string.Empty))
                .ForMember(dest => dest.Date, opt => opt.MapFrom(src => src.Date))
                .ForMember(dest => dest.Hour, opt => opt.MapFrom(src => Convert.ToInt32(src.Hour)))
                .ForMember(dest => dest.Count, opt => opt.MapFrom(src => src.In_Count))
                //.ForMember(dest => dest.Count, opt => opt.MapFrom(src => src.Out_Count))
                .ForMember(dest => dest.DoorDateKey, opt => opt.MapFrom(src => $"{customerModel.CustomerCID ?? string.Empty }{src.Date.ToString("yyyyMMdd")}{src.Hour}"))
                .ForMember(dest => dest.VendorName, opt => opt.MapFrom(src => "Dor")));

            return config.CreateMapper().Map<IEnumerable<DoorCounterModel>>(models);
        }

        public DoorCounterModel ConvertToDoorCounterModel(FootTrafficDataModel model)
        {
            var customerModel = _customerRepository.FindByStoreId(model.Store_Id);

            var config = new MapperConfiguration(cfg => cfg.CreateMap<FootTrafficDataModel, DoorCounterModel>()
                .ForMember(dest => dest.SiteId, opt => opt.MapFrom(src => src.Store_Id))
                .ForMember(dest => dest.CustomerCID, opt => opt.MapFrom(src => customerModel.CustomerCID))
                .ForMember(dest => dest.CustomerName, opt => opt.MapFrom(src => customerModel.CustomerName))
                .ForMember(dest => dest.Date, opt => opt.MapFrom(src => src.Date))
                .ForMember(dest => dest.Hour, opt => opt.MapFrom(src => Convert.ToInt32(src.Hour)))
                .ForMember(dest => dest.Count, opt => opt.MapFrom(src => src.In_Count))
                //.ForMember(dest => dest.Count, opt => opt.MapFrom(src => src.Out_Count))
                .ForMember(dest => dest.DoorDateKey, opt => opt.MapFrom(src => $"{customerModel.CustomerCID}{src.Date.ToString("yyyyMMdd")}{src.Hour}"))
                .ForMember(dest => dest.VendorName, opt => opt.MapFrom(src => "Dor")));

            return config.CreateMapper().Map<DoorCounterModel>(model);
        }

        private List<DoorCounterModel> AddMissingHours(IEnumerable<DoorCounterModel> models)
        {
            var doorCounterList = models.ToList();
            var defaultModel = doorCounterList.First();

            //assuming starting hour is 01
            for (var hour = 1; hour <= 24; hour++)
            {
                var doorCounterModel = models.Where(m => m.Hour == hour).Select(m => m).SingleOrDefault();

                if (doorCounterModel == null)
                {
                    doorCounterModel = new DoorCounterModel
                    {
                        SiteId = defaultModel.SiteId,
                        CustomerCID = defaultModel.CustomerCID,
                        CustomerName = defaultModel.CustomerName,
                        Date = defaultModel.Date,
                        Hour = hour,
                        Count = 0,
                        DoorDateKey = $"{defaultModel.CustomerCID}{defaultModel.Date.ToString("yyyyMMdd")}{hour.ToString().PadLeft(2, '0')}",
                        VendorName = "Dor"
                    };

                    doorCounterList.Add(doorCounterModel);
                }
            }

            return doorCounterList;
        }
    }
}
