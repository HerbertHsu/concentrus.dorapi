﻿using System;
using DataLayer.Models.Dor;
using DataLayer.Repository.Interface;
using DataLayer.Services.Interface;
using NLog;

namespace DataLayer.Services.Implementation
{
    public class TeamService: ITeamService
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();
        private ITeamRepository _teamRepo;

        public TeamService(ITeamRepository teamRepo)
        {
            _teamRepo = teamRepo;
        }

        public void Add(TeamModel model)
        {
            try
            {
                var isExists = _teamRepo.IsExist(model.Id);

                if (!isExists)
                {
                    _teamRepo.Add(model);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Team Service | Add | exception message: {0}", ex.Message);
            }
        }
    }
}
