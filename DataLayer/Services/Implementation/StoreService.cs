﻿using System;
using System.Linq;
using DataLayer.Models.Dor;
using DataLayer.Repository.Interface;
using DataLayer.Services.Interface;
using Newtonsoft.Json;
using NLog;

namespace DataLayer.Services.Implementation
{
    public class StoreService : IStoreService
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();
        private readonly IStoreRepository _storeRepo;
        private readonly IAddressRepository _addressRepo;
        private readonly IDeviceRepository _deviceRepo;
        private readonly IDevicesRepository _devicesRepo;

        public StoreService(IStoreRepository storeRepo, IAddressRepository addressRepo, IDeviceRepository deviceRepo, IDevicesRepository devicesRepo)
        {
            _storeRepo = storeRepo;
            _addressRepo = addressRepo;
            _deviceRepo = deviceRepo;
            _devicesRepo = devicesRepo;
        }

        public void Add(StoreModel model)
        {
            try
            {
                var isStoreExists = _storeRepo.IsExist(model.Id);

                if (isStoreExists) return;

                //Address
                var isAddressExists = _addressRepo.IsExist(model.Address.Id);
                if (!isAddressExists)
                {
                    Logger.Debug("StoreService | model.Address: {0}", JsonConvert.SerializeObject(model.Address));
                    _addressRepo.Add(model.Address);
                }

                //Shipping_Address (Address)
                var isShippingAddressExists = _addressRepo.IsExist(model.Shipping_Address.Id);
                if (!isShippingAddressExists)
                {
                    Logger.Debug("StoreService | model.Shipping_Address: {0}", JsonConvert.SerializeObject(model.Shipping_Address));
                    _addressRepo.Add(model.Shipping_Address);
                }

                //Device
                foreach (var deviceModel in
                         from deviceModel in model.Devices
                         let isDeviceExists = _deviceRepo.IsExists(deviceModel)
                         where !isDeviceExists
                         select deviceModel)
                {
                    Logger.Debug("StoreService | deviceModel: {0}", JsonConvert.SerializeObject(deviceModel));
                    _deviceRepo.Add(deviceModel);
                }

                //Devices
                var devicesId = (!_devicesRepo.IsExist(model.Id, model.Devices)) ? _devicesRepo.Add(model.Id, model.Devices) : _devicesRepo.GetDevicesId(model.Id, model.Devices);
                _storeRepo.Add(model, devicesId);

                Logger.Debug("StoreService | Add | model.Id: {0}", model.Id);
                Logger.Debug("StoreService | Add | model.Devices: {0}", JsonConvert.SerializeObject(model.Devices));
                Logger.Debug("StoreService | Add | devicesId: {0}", devicesId);
            }
            catch (Exception ex)
            {
                Logger.Error("Store Service | Add | exception message: {0}", ex.Message);
            }
        }
    }
}
