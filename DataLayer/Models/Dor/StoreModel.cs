﻿using System.Collections.Generic;

namespace DataLayer.Models.Dor
{
    public class StoreModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Sensor_Count { get; set; }

        public int Team_Id { get; set; }

        public int User_Id { get; set; }

        public AddressModel Address { get; set; }

        public AddressModel Shipping_Address { get; set; }

        public IEnumerable<DeviceModel> Devices { get; set; }
    }
}