﻿namespace DataLayer.Models.Dor
{
    public class AddressModel
    {
        public string Address_1 { get; set; }

        public string Address_2 { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public string State { get; set; }

        public string Tz_Name { get; set; }

        public string ZipCode { get; set; }
    }
}