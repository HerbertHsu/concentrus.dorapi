﻿namespace DataLayer.Models.Dor
{
    public class TeamModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}