﻿namespace DataLayer.Models.Dor
{
    public class DevicesModel
    {
        public int Id { get; set; }

        public string SerialNumber { get; set; }

        public int StoreId { get; set; }
    }
}
