﻿namespace DataLayer.Models.Dor
{
    public class CustomerModel
    {
        public string Dor_Account_ID { get; set; }

        public string CustomerID { get; set; }

        public string CustomerCID { get; set; }

        public string CustomerName { get; set; }
    }
}
