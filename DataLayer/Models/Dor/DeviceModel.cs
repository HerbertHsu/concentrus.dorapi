﻿namespace DataLayer.Models.Dor
{
    public class DeviceModel
    {
        public string Serial_Number { get; set; }

        public string Created_At { get; set; }

        public string Deleted_At { get; set; }

        public string Device_Type_Name { get; set; }
    }
}