﻿using System;

namespace DataLayer.Models.Dor
{
    public class FootTrafficDataModel
    {
        public int Id { get; set; }

        public int Store_Id { get; set; }

        public DateTime Date { get; set; }

        public string Datetime { get; set; }

        public string Hour { get; set; }

        public int In_Count { get; set; }

        public int Out_Count { get; set; }
    }
}