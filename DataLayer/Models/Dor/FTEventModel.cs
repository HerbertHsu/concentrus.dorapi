﻿namespace DataLayer.Models.Dor
{
    public class FTEventModel
    {
        public string Date { get; set; }

        public int In_Count { get; set; }

        public int Out_Count { get; set; }

        public int Store_Id { get; set; }
    }
}