﻿using System.Collections.Generic;
using System.Linq;
using Dapper;
using DataLayer.Models.Dor;
using DataLayer.Repository.Interface;

namespace DataLayer.Repository.Implementation
{
    public class AddressRepository : BaseRepository, IAddressRepository
    {
        public AddressModel Find(int id)
        {
            return this._dbConnection.Query<AddressModel>("SELECT * FROM DorAddress WHERE Id = @Id", new { Id = id }).SingleOrDefault();
        }

        public List<AddressModel> GetAll()
        {
            return this._dbConnection.Query<AddressModel>("Select * From DorAddress").ToList();
        }

        public void Add(AddressModel model)
        {
            var sql =
                "INSERT INTO DorAddress (Id, Name, Address_1, Address_2, City, State, Country, Zipcode, Tz_Name) " +
                "VALUES(@Id, @Name, @Address_1, @Address_2, @City, @State, @Country, @Zipcode, @Tz_Name); ";

            this._dbConnection.Execute(sql, model);
        }

        public AddressModel FindByName(string name)
        {
            return this._dbConnection.Query<AddressModel>("SELECT * FROM DorAddress WHERE Name = @Name", new { Name = name }).SingleOrDefault();
        }

        public bool IsExist(int id)
        {
            var sql =
                "SELECT COUNT(*) " +
                "FROM DorAddress " +
                "WHERE Id = @Id";

            int count = this._dbConnection.ExecuteScalar<int>(sql, new { Id = id });

            return (count > 0);
        }
    }
}
