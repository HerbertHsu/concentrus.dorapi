﻿using System.Linq;
using Dapper;
using DataLayer.Models.Dor;
using DataLayer.Repository.Interface;

namespace DataLayer.Repository.Implementation
{
    public class CustomerRepository : BaseRepository, ICustomerRepository
    {
        public CustomerModel FindByStoreId(int storeId)
        {
            //const string sql = "SELECT * FROM Customers WHERE DOR_Account_ID = @StoreId";
            const string sql = "SELECT TOP 1 * FROM vCustomers WHERE DOR_Account_ID = @StoreId";
            return _dbConnection.Query<CustomerModel>(sql, new { StoreId = storeId }).SingleOrDefault();
        }

        public bool IsExist(int storeId)
        {
            //const string sql = "SELECT COUNT(*) " +
            //                   "FROM Customers " +
            //                   "WHERE DOR_Account_ID = @StoreId";
            const string sql = "SELECT COUNT(*) " +
                               "FROM vCustomers " +
                               "WHERE DOR_Account_ID = @StoreId";

            var count = _dbConnection.ExecuteScalar<int>(sql, new { StoreId = storeId });
            return (count > 0);
        }
    }
}
