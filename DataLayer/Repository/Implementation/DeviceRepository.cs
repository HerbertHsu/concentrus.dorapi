﻿using System.Collections.Generic;
using System.Linq;
using Dapper;
using DataLayer.Models.Dor;
using DataLayer.Repository.Interface;

namespace DataLayer.Repository.Implementation
{
    public class DeviceRepository : BaseRepository, IDeviceRepository
    {
        public DeviceModel FindBySerialNumber(string serialNumber)
        {
            const string sql = "SELECT * FROM DorDevice WHERE Serial_Number = @SerialNumber";
            return _dbConnection.Query<DeviceModel>(sql, new { SerialNumber = serialNumber }).SingleOrDefault();
        }

        public List<DeviceModel> GetAll()
        {
            const string sql = "Select * From DorDevice";
            return _dbConnection.Query<DeviceModel>(sql).ToList();
        }

        public void Add(DeviceModel model)
        {
            const string sql = "INSERT INTO DorDevice (Created_At, Deleted_At, Serial_Number, Device_Type_Name) " +
                               "VALUES(@Created_At, @Deleted_At, @Serial_Number, @Device_Type_Name); ";

            _dbConnection.Execute(sql, model);
        }

        public void AddBulk(IEnumerable<DeviceModel> models)
        {
            const string sql = "INSERT INTO DorDevice " +
                               "VALUES(@Serial_Number, @Created_At, @Deleted_At, @Device_Type_Name); ";

            _dbConnection.Execute(sql, models);
        }

        public bool IsExists(DeviceModel model)
        {
            const string sql = "SELECT COUNT(*) " +
                               "FROM DorDevice " +
                               "WHERE Serial_Number = @SerialNumber";

            var count = _dbConnection.ExecuteScalar<int>(sql, new { SerialNumber = model.Serial_Number });

            return (count > 0);
        }
    }
}
