﻿using System.Collections.Generic;
using System.Linq;
using Dapper;
using DataLayer.Models.Dor;
using DataLayer.Repository.Interface;

namespace DataLayer.Repository.Implementation
{
    public class StoreRepository : BaseRepository, IStoreRepository
    {
        public StoreModel Find(int id)
        {
            return this._dbConnection.Query<StoreModel>("SELECT * FROM DorStore WHERE Id = @Id", new { Id = id }).SingleOrDefault();
        }

        public List<StoreModel> GetAll()
        {
            return this._dbConnection.Query<StoreModel>("Select * From DorStore").ToList();
        }

        public void Add(StoreModel model, int devicesId)
        {
            var sql =
                "INSERT INTO DorStore (Id, Name, Sensor_Count, AddressId, Team_Id, ShippingAddressId, User_Id, DevicesId) " +
                "VALUES(@Id, @Name, @Sensor_Count, @AddressId, @Team_Id, @ShippingAddressId, @User_Id, @DevicesId); ";

            this._dbConnection.Execute(sql, new
            {
                Id = model.Id,
                Name = model.Name,
                Sensor_Count = model.Sensor_Count,
                AddressId = model.Address.Id,
                Team_Id = model.Team_Id,
                ShippingAddressId = model.Shipping_Address.Id,
                User_Id = model.User_Id,
                DevicesId = devicesId
            });
        }

        public StoreModel FindByName(string name)
        {
            return this._dbConnection.Query<StoreModel>("SELECT * FROM DorStore WHERE Name = @Name", new { Name = name }).SingleOrDefault();
        }

        public bool IsExist(int id)
        {
            var sql =
                "SELECT COUNT(*) " +
                "FROM DorStore " +
                "WHERE Id = @Id";

            int count = this._dbConnection.ExecuteScalar<int>(sql, new { Id = id });

            return (count > 0);
        }

        public StoreModel FindByUserId(int userId)
        {
            return this._dbConnection.Query<StoreModel>("SELECT * FROM DorStore WHERE User_Id = @UserId", new { UserId = userId }).SingleOrDefault();
        }
    }
}
