﻿using System.Collections.Generic;
using System.Linq;
using Dapper;
using DataLayer.Models.Dor;
using DataLayer.Repository.Interface;

namespace DataLayer.Repository.Implementation
{
    public class TeamRepository : BaseRepository, ITeamRepository
    {
        public TeamModel Find(int id)
        {
            return this._dbConnection.Query<TeamModel>("SELECT * FROM DorTeam WHERE Id = @Id", new { Id = id }).SingleOrDefault();
        }

        public List<TeamModel> GetAll()
        {
            return this._dbConnection.Query<TeamModel>("Select * From DorTeam").ToList();
        }

        public void Add(TeamModel model)
        {
            var sql =
                "INSERT INTO DorTeam (Id, Name) " +
                "VALUES(@Id, @Name); ";

            this._dbConnection.Execute(sql, model);
        }

        public TeamModel FindByName(string name)
        {
            return this._dbConnection.Query<TeamModel>("SELECT * FROM DorTeam WHERE Name = @Name", new { Name = name }).SingleOrDefault();
        }

        public bool IsExist(int id)
        {
            var sql = 
                "SELECT COUNT(*) " +
                "FROM DorTeam " + 
                "WHERE Id = @Id";

            int count = this._dbConnection.ExecuteScalar<int>(sql, new { Id = id });

            return (count > 0);
        }
    }
}
