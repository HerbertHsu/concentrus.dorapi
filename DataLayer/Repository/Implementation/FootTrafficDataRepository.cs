﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using DataLayer.Models.Dor;
using DataLayer.Repository.Interface;

namespace DataLayer.Repository.Implementation
{
    public class FootTrafficDataRepository : BaseRepository, IFootTrafficDataRepository
    {
        public FootTrafficDataModel Find(int id)
        {
            return this._dbConnection.Query<FootTrafficDataModel>("SELECT * FROM DorFootTrafficData WHERE Id = @Id", new { Id = id }).SingleOrDefault();
        }

        public List<FootTrafficDataModel> FindByStoreId(int storeId)
        {
            return this._dbConnection.Query<FootTrafficDataModel>("SELECT * FROM DorFootTrafficData WHERE Store_Id = @StoreId ", new { StoreId = storeId }).ToList();
        }

        public List<FootTrafficDataModel> FindByDate(DateTime date)
        {
            return this._dbConnection.Query<FootTrafficDataModel>("SELECT * FROM DorFootTrafficData WHERE Date = @Date", new { Date = date.ToShortDateString() }).ToList();
        }

        public List<FootTrafficDataModel> FindByStoreAndDate(int storeId, DateTime date)
        {
            return this._dbConnection.Query<FootTrafficDataModel>("SELECT * FROM DorFootTrafficData WHERE Store_Id = @StoreId AND Date = @Date", new { StoreId = storeId, Date = date.ToShortDateString() }).ToList();
        } 

        public void Add(FootTrafficDataModel model)
        {
            var sql =
                "INSERT INTO DorFootTrafficData (Store_Id, Date, Datetime, Hour, In_Count, Out_Count) " +
                "VALUES(@StoreId, @Date, @Datetime, @Hour, @InCount, @OutCount); ";

            this._dbConnection.Execute(sql, new
            {
                StoreId = model.Store_Id,
                Date = model.Date.ToShortDateString(),
                DateTime = model.Datetime,
                Hour = model.Hour,
                InCount = model.In_Count,
                OutCount = model.Out_Count
            });
        }
    }
}
