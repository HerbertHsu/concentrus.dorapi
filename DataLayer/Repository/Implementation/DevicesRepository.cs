﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Dapper;
using DataLayer.Models.Dor;
using DataLayer.Repository.Interface;

namespace DataLayer.Repository.Implementation
{
    public class DevicesRepository : BaseRepository, IDevicesRepository
    {
        public List<DevicesModel> Find(int storeId)
        {
            return this._dbConnection.Query<DevicesModel>("SELECT * FROM DorDevices WHERE StoreId = @StoreId", new { StoreId = storeId }).ToList();
        }

        public int Add(int storeId, IEnumerable<DeviceModel> models)
        {
            var sql =
                "SELECT ISNULL(MAX(Id), 0) " +
                "FROM DorDevices ";

            var devicesId = this._dbConnection.ExecuteScalar<int>(sql);
            devicesId = devicesId + 1;

            var config = new MapperConfiguration(cfg => cfg.CreateMap<DeviceModel, DevicesModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => devicesId))
                .ForMember(dest => dest.StoreId, opt => opt.MapFrom(src => storeId))
                .ForMember(dest => dest.SerialNumber, opt => opt.MapFrom(src => src.Serial_Number)));

            var devices = config.CreateMapper().Map<IEnumerable<DevicesModel>>(models).ToList();

            sql =
                "INSERT INTO DorDevices " +
                "VALUES(@Id, @SerialNumber, @StoreId); ";

            _dbConnection.Execute(sql, devices);

            return devicesId;
        }

        public bool IsExist(int storeId, IEnumerable<DeviceModel> models)
        {
            var sql =
                "SELECT COUNT(*) " +
                "FROM DorDevices " +
                "WHERE StoreId = @StoreId " + 
                "AND SerialNumber IN @SerialNumbers";

            var count = this._dbConnection.ExecuteScalar<int>(sql, new { StoreId = storeId, SerialNumbers = models.Select(s => s.Serial_Number).ToArray() });

            return (count > 0);
        }

        public int GetDevicesId(int storeId, IEnumerable<DeviceModel> models)
        {
            var sql =
                "SELECT DISTINCT Id" +
                "FROM DorDevices " +
                "WHERE StoreId = @StoreId " +
                "AND SerialNumber IN @SerialNumbers";

            return _dbConnection.ExecuteScalar<int>(sql, new { StoreId = storeId, SerialNumbers = models.Select(s => s.Serial_Number).ToArray() });
        }
    }
}
