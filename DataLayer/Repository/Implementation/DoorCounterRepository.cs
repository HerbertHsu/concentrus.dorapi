﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer.Repository.Interface;
using DataLayer.Models.DoorCounter;
using Dapper;

namespace DataLayer.Repository.Implementation
{
    public class DoorCounterRepository : BaseRepository, IDoorCounterRepository
    {
        public DoorCounterModel Find(int id)
        {
            return this._dbConnection.Query<DoorCounterModel>("SELECT * FROM DoorCounterReport WHERE Id = @Id AND VendorName = 'DOR'", new { Id = id }).SingleOrDefault();
        }

        public List<DoorCounterModel> FindByStoreId(int storeId)
        {
            return this._dbConnection.Query<DoorCounterModel>("SELECT * FROM DoorCounterReport WHERE SiteId = @StoreId AND VendorName = 'DOR'", new { StoreId = storeId }).ToList();
        }

        public List<DoorCounterModel> FindByCustomerCID(string customerCID)
        {
            return this._dbConnection.Query<DoorCounterModel>("SELECT * FROM DoorCounterReport WHERE CustomerCID = @CustomerCID AND VendorName = 'DOR'", new { CustomerCID = customerCID }).ToList();
        }

        public List<DoorCounterModel> FindByCustomerName(string customerName)
        {
            return this._dbConnection.Query<DoorCounterModel>("SELECT * FROM DoorCounterReport WHERE CustomerName = @CustomerName AND VendorName = 'DOR'", new { CustomerName = customerName }).ToList();
        }

        public List<DoorCounterModel> FindByDate(DateTime date)
        {
            return this._dbConnection.Query<DoorCounterModel>("SELECT * FROM DoorCounterReport WHERE Date = @Date AND VendorName = 'DOR'", new { Date = date.ToString("yyyy-MM-dd") }).ToList();
        }

        public List<DoorCounterModel> GetAll()
        {
            return this._dbConnection.Query<DoorCounterModel>("Select * From DoorCounterReport AND VendorName = 'DOR'").ToList();
        }

        public void Add(DoorCounterModel model)
        {
            var sql =
                "INSERT INTO DoorCounterReport (SiteId, CustomerCID, CustomerName, Date, Hour, Count, DoorDateKey, VendorName) " +
                "VALUES(@SiteId, @CustomerCID, @CustomerName, @Date, @Hour, @Count, @DoorDateKey, @VendorName); ";

            this._dbConnection.Execute(sql, model);
        }

        public void AddBulk(List<DoorCounterModel> models)
        {
            var sql =
                "INSERT INTO DoorCounterReport " +
                "VALUES(@SiteId, @CustomerCID, @CustomerName, @Date, @Hour, @Count, @DoorDateKey, @VendorName); ";

            this._dbConnection.Execute(sql, models);
        }
    }
}