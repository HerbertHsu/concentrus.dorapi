﻿using System.Data;
using System.Data.SqlClient;

namespace DataLayer.Repository
{
    public abstract class BaseRepository
    {
        public IDbConnection _dbConnection;

        public BaseRepository()
        {
            _dbConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["doorCounterDB"].ConnectionString);
        }
    }
}
