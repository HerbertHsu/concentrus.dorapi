﻿using System.Collections.Generic;
using DataLayer.Models.Dor;

namespace DataLayer.Repository.Interface
{
    public interface IStoreRepository
    {
        StoreModel Find(int id);

        List<StoreModel> GetAll();

        void Add(StoreModel model, int devicesId);

        StoreModel FindByName(string name);

        bool IsExist(int id);

        StoreModel FindByUserId(int userId);
    }
}