﻿using System.Collections.Generic;
using DataLayer.Models.Dor;

namespace DataLayer.Repository.Interface
{
    public interface IAddressRepository
    {
        AddressModel Find(int id);

        List<AddressModel> GetAll();

        void Add(AddressModel model);

        AddressModel FindByName(string name);

        bool IsExist(int id);
    }
}