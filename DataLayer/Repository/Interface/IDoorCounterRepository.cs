﻿using System;
using System.Collections.Generic;
using DataLayer.Models.DoorCounter;

namespace DataLayer.Repository.Interface
{
    public interface IDoorCounterRepository
    {
        DoorCounterModel Find(int id);

        List<DoorCounterModel> FindByStoreId(int storeId);

        List<DoorCounterModel> FindByCustomerCID(string customerCID);

        List<DoorCounterModel> FindByCustomerName(string customerName);

        List<DoorCounterModel> FindByDate(DateTime date);

        List<DoorCounterModel> GetAll();

        void Add(DoorCounterModel dcModel);

        void AddBulk(List<DoorCounterModel> dcModel);
    }
}
