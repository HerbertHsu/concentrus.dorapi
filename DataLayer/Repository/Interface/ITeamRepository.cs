﻿using System.Collections.Generic;
using DataLayer.Models.Dor;

namespace DataLayer.Repository.Interface
{
    public interface ITeamRepository
    {
        TeamModel Find(int id);

        List<TeamModel> GetAll();

        void Add(TeamModel model);

        TeamModel FindByName(string name);

        bool IsExist(int id);
    }
}