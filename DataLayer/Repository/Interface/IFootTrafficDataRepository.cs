﻿using System;
using System.Collections.Generic;
using DataLayer.Models.Dor;

namespace DataLayer.Repository.Interface
{
    public interface IFootTrafficDataRepository
    {
        FootTrafficDataModel Find(int id);

        List<FootTrafficDataModel> FindByStoreId(int storeId);

        List<FootTrafficDataModel> FindByDate(DateTime date);

        List<FootTrafficDataModel> FindByStoreAndDate(int storeId, DateTime date);

        void Add(FootTrafficDataModel model);
    }
}