﻿using DataLayer.Models.Dor;

namespace DataLayer.Repository.Interface
{
    public interface ICustomerRepository
    {
        CustomerModel FindByStoreId(int storeId);

        bool IsExist(int storeId);
    }
}
