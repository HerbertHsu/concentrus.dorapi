﻿using System.Collections.Generic;
using DataLayer.Models.Dor;

namespace DataLayer.Repository.Interface
{
    public interface IDevicesRepository
    {
        List<DevicesModel> Find(int storeId);

        int Add(int storeId, IEnumerable<DeviceModel> model);

        bool IsExist(int storeId, IEnumerable<DeviceModel> models);

        int GetDevicesId(int storeId, IEnumerable<DeviceModel> models);
    }
}