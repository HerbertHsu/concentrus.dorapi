﻿using System.Collections.Generic;
using DataLayer.Models.Dor;

namespace DataLayer.Repository.Interface
{
    public interface IDeviceRepository
    {
        DeviceModel FindBySerialNumber(string serialNumber);

        List<DeviceModel> GetAll();

        void Add(DeviceModel model);

        void AddBulk(IEnumerable<DeviceModel> models);

        bool IsExists(DeviceModel model);
    }
}