﻿using System;
using DataLayer.Models.Dor;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataLayer.Repository.Interface;
using DataLayer.Services.Interface;
using FluentAssertions;

namespace DataLayer.Tests
{
    [TestClass]
    public class TeamRepositoryTests
    {
        [TestMethod]
        public void Test_Query()
        {
            // Arrange
            ITeamRepository repository = CreateRepository();
            var model = new TeamModel
            {
                Id = 1098,
                Name = "Concentrus"
            };

            // Act
            var newAddedModel = repository.Find(model.Id);

            // Assert
            newAddedModel.Id.ShouldBeEquivalentTo(model.Id);
            Console.WriteLine("newAddedModel ID: " + newAddedModel.Id);
        }

        [TestMethod]
        public void Insert_Should_Assign_Identity_To_New_Entity()
        {
            // Arrange
            ITeamRepository repository = CreateRepository();
            var model = new TeamModel
            {
                Id = 1098,
                Name = "Concentrus"
            };

            // Act
            repository.Add(model);
            // repository.Save(contact);

            // Assert
            var newAddedModel = repository.Find(model.Id);
            newAddedModel.Id.ShouldBeEquivalentTo(model.Id);
            Console.WriteLine("model ID: " + model.Id);
            Console.WriteLine("newAddedModel ID: " + newAddedModel.Id);
        }

        [TestMethod]
        public void Team_Concentrus_Exists()
        {
            //Arrange
            ITeamRepository repository = CreateRepository();
            var model = new TeamModel
            {
                Id = 1098,
                Name = "Concentrus"
            };

            //Act
            var isExist = repository.IsExist(model.Id);

            //Assert
            isExist.Should().BeTrue();
        }

        [TestMethod]
        public void Team_Concentrus_Does_Not_Exists()
        {
            //Arrange
            ITeamRepository repository = CreateRepository();
            var model = new TeamModel
            {
                Id = 1098,
                Name = "Concentrus"
            };

            //Act
            var isExist = repository.IsExist(model.Id);

            //Assert
            isExist.Should().BeFalse();
        }

        private ITeamRepository CreateRepository()
        {
            return new Repository.Implementation.TeamRepository();
        }
    }
}
