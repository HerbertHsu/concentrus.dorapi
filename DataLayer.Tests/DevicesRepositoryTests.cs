﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer.Models.Dor;
using DataLayer.Repository.Interface;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataLayer.Tests
{
    [TestClass]
    public class DevicesRepositoryTests
    {
        [TestMethod]
        public void Add_Devices_Should_Be_Successful()
        {
            // Arrange
            IDevicesRepository repository = CreateRepository();

            List<DeviceModel> models = new List<DeviceModel>
            {
                new DeviceModel { Created_At = "20161228", Deleted_At = "", Device_Type_Name = "lol", Serial_Number = "aaa" },
                new DeviceModel { Created_At = "20161228", Deleted_At = "", Device_Type_Name = "lol", Serial_Number = "bbb" },
                new DeviceModel { Created_At = "20161228", Deleted_At = "", Device_Type_Name = "lol", Serial_Number = "ccc" },
            };

            // Act
            if (!repository.IsExist(1, models))
            {
                var devices = repository.Add(1, models);
            }

            List<DeviceModel> models2 = new List<DeviceModel>
            {
                new DeviceModel { Created_At = "20161228", Deleted_At = "", Device_Type_Name = "lol", Serial_Number = "aaa" },
                new DeviceModel { Created_At = "20161228", Deleted_At = "", Device_Type_Name = "lol", Serial_Number = "ccc" },
            };

            // Act
            if (!repository.IsExist(1, models2))
            {
                var devices = repository.Add(1, models2);
            }

            //Assert
        }

        private IDevicesRepository CreateRepository()
        {
            return new Repository.Implementation.DevicesRepository();
        }
    }
}
