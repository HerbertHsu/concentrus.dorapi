﻿using System;
using DataLayer.Extensions;
using DataLayer.Models.Dor;
using DataLayer.Repository.Interface;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataLayer.Tests
{
    [TestClass]
    public class CustomerRepositoryTests
    {
        [TestMethod]
        public void TestFindByStoreId()
        {
            // Arrange
            var repo = CreateRepository();

            // Act
            const int storeId = 1140;
            var customerModel = repo.FindByStoreId(storeId).Mapping<CustomerModel>();

            Console.WriteLine(customerModel.CustomerID);
            Console.WriteLine(customerModel.CustomerCID);
            Console.WriteLine(customerModel.CustomerName);
            Console.WriteLine(customerModel.Dor_Account_ID);

            // Assert
            customerModel.CustomerID.Should().Be("66903");
            customerModel.CustomerCID.Should().Be("C16848S1");
            customerModel.CustomerName.Should().Be("OKJOHNLEE LLC");
            customerModel.Dor_Account_ID.Should().Be("1140");
        }

        private ICustomerRepository CreateRepository()
        {
            return new Repository.Implementation.CustomerRepository();
        }
    }
}
