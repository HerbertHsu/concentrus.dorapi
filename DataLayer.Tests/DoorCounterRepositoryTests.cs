﻿using System;
using DataLayer.Models.DoorCounter;
using DataLayer.Repository.Interface;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataLayer.Tests
{
    [TestClass]
    public class DoorCounterRepositoryTests
    {
        [TestMethod]
        public void Get_All_Should_Return_456_Results()
        {
            // Arrange
            IDoorCounterRepository repository = CreateRepository();

            // Act
            var counters = repository.GetAll();

            // Assert
            counters.Should().NotBeNull();
            counters.Count.Should().Be(456);
        }

        static int id;

        [TestMethod]
        public void Insert_Should_Assign_Identity_To_New_Entity()
        {
            // Arrange
            IDoorCounterRepository repository = CreateRepository();
            var dcModel = new DoorCounterModel
            {
                SiteId = 13,
                CustomerCID = "C13538S2",
                CustomerName = "Door Counter Customer",
                Date = Convert.ToDateTime("2016-12-12"),
                Hour = 5,
                Count = 26
            };

            dcModel.DoorDateKey = $"{dcModel.CustomerCID}{dcModel.Date.Year}{dcModel.Date.Month.ToString("00")}{dcModel.Date.Day.ToString("00")}{dcModel.Hour.ToString("00")}";

            // Act
            repository.Add(dcModel);
            // repository.Save(contact);

            // Assert
            dcModel.Id.Should().NotBe(0, "because Identity should have been assigned by database.");
            Console.WriteLine("New ID: " + dcModel.Id);
            id = dcModel.Id;
        }

        [TestMethod]
        public void Find_Should_Retrieve_Existing_Entity()
        {
            // Arrange
            IDoorCounterRepository repository = CreateRepository();

            // Act
            var dcModel = repository.Find(id);

            // Assert
            dcModel.Should().NotBeNull();
            dcModel.Id.Should().Be(id);
            dcModel.SiteId.Should().Be(13);
            dcModel.CustomerCID.Should().Be("C13538S2");
            dcModel.CustomerName.Should().Be("Door Counter Customer");
            dcModel.Date.Should().Be(Convert.ToDateTime("2016-12-12"));
            dcModel.Hour.Should().Be(5);
            dcModel.Count.Should().Be(26);
        }

        private IDoorCounterRepository CreateRepository()
        {
            return new Repository.Implementation.DoorCounterRepository();
        }
    }
}
