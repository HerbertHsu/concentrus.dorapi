﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer.Models.Dor;
using DataLayer.Services.Interface;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataLayer.Tests
{
    [TestClass]
    public class DoorCounterServiceTests
    {
        private readonly Resolver _resolver = new Resolver();

        [TestMethod]
        public void TestMethod1()
        {
            IDoorCounterService service = _resolver.Resolve<IDoorCounterService>();

            IEnumerable<FootTrafficDataModel> dataModels = new List<FootTrafficDataModel>
            {
                new FootTrafficDataModel { Datetime = "2016-12-28 07", Date = Convert.ToDateTime("2016-12-28"), Hour = "07", In_Count = 2, Out_Count = 0, Store_Id = 1098},
                new FootTrafficDataModel { Datetime = "2016-12-28 23", Date = Convert.ToDateTime("2016-12-28"), Hour = "23", In_Count = 8, Out_Count = 0, Store_Id = 1098},
                new FootTrafficDataModel { Datetime = "2016-12-28 24", Date = Convert.ToDateTime("2016-12-28"), Hour = "24", In_Count = 1, Out_Count = 0, Store_Id = 1098}
            };

            //var hours = (from d in dataModels
            //             select Convert.ToInt32(d.Hour)).ToArray();

            //int[] hoursPerDay = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 };

            service.AddBulk(dataModels);
        }
    }
}
