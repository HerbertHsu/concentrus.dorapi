﻿using Autofac;
using DataLayer.Repository.Implementation;
using DataLayer.Repository.Interface;
using DataLayer.Services.Implementation;
using DataLayer.Services.Interface;

namespace DataLayer.Tests
{
    public class Resolver
    {
        private readonly IContainer _container;
        public Resolver()
        {
            var builder = new ContainerBuilder();
            RegisterTypes(builder);
            _container = builder.Build();
        }

        private static void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterType<DoorCounterService>().As<IDoorCounterService>();
            builder.RegisterType<DoorCounterRepository>().As<IDoorCounterRepository>();
            builder.RegisterType<CustomerRepository>().As<ICustomerRepository>();
        }

        public T Resolve<T>()
        {
            return _container.Resolve<T>();
        }
    }
}