﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Web.Http;
using Concentrus.DorAPI.Controllers;
using Concentrus.DorAPI.Models.Dor;
using Concentrus.DorAPI.WebAPIs;
using FakeItEasy;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Concentrus.DorAPI.Tests
{
    [TestClass]
    public class DorControllerTests
    {
        [TestMethod]
        public void Get_Teams_Should_Be_Called()
        {
            // Arrange
            var dorApi = A.Fake<IDorAPI>();
            var controller = new DorController(dorApi);

            // Act
            controller.GetTeams();

            // Assert
            //A.CallTo(() => dorApi.GetTeams()).MustHaveHappened(Repeated.Exactly.Once);
        }

        [TestMethod]
        public void Get_Teams_Results_Should_Have_Data_And_Meta_Properties()
        {
            // Arrange
            string fileName = "teams.json";
            JObject responseJObject = GetJsonFile(fileName);

            var dorApi = A.Fake<IDorAPI>();
            var controller = new DorController(dorApi);

            //HttpResponseMessage response = controller.GetTeams().As<HttpResponseMessage>();
            //A.CallTo(() => response).Returns(responseJObject);

            // Act
            //controller.GetTeams();
            //var response = dorApi.GetTeams().ToJObject();
            //Console.WriteLine(response);

            // Assert
            //A.CallTo(() => response).Should().NotBeNull();
        }

        private JObject GetJsonFile(string fileName)
        {
            string fullPathFileName = $"{@"..\..\Json\"}{fileName}";
            JObject responseJObject = new JObject();

            using (StreamReader file = File.OpenText(fullPathFileName))
            {
                using (JsonTextReader reader = new JsonTextReader(file))
                {
                    responseJObject = (JObject)JToken.ReadFrom(reader);
                }
            }

            Console.WriteLine(responseJObject);

            return responseJObject;
        }
    }
}
