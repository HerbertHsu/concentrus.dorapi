﻿using Autofac;
using Concentrus.DorAPI.WebAPIs;
using DataLayer.Repository.Implementation;
using DataLayer.Repository.Interface;
using DataLayer.Services.Implementation;
using DataLayer.Services.Interface;

namespace Concentrus.DorAPI.Tests
{
    public class Resolver
    {
        private readonly IContainer _container;
        public Resolver()
        {
            var builder = new ContainerBuilder();
            RegisterTypes(builder);
            _container = builder.Build();
        }

        private static void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterType<TeamService>().As<ITeamService>();
            builder.RegisterType<StoreService>().As<IStoreService>();
            builder.RegisterType<FootTrafficDataService>().As<IFootTrafficDataService>();
            builder.RegisterType<DoorCounterService>().As<IDoorCounterService>();

            builder.RegisterType<AddressRepository>().As<IAddressRepository>();
            builder.RegisterType<CustomerRepository>().As<ICustomerRepository>();
            builder.RegisterType<DeviceRepository>().As<IDeviceRepository>();
            builder.RegisterType<DevicesRepository>().As<IDevicesRepository>();
            builder.RegisterType<DoorCounterRepository>().As<IDoorCounterRepository>();
            builder.RegisterType<FootTrafficDataRepository>().As<IFootTrafficDataRepository>();
            builder.RegisterType<StoreRepository>().As<IStoreRepository>();
            builder.RegisterType<TeamRepository>().As<ITeamRepository>();

            builder.RegisterType<BaseAPI>().As<IBaseAPI>();
            builder.RegisterType<DorApi>().As<IDorAPI>();
        }

        public T Resolve<T>()
        {
            return _container.Resolve<T>();
        }
    }
}
