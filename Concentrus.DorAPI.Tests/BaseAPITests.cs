﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Concentrus.DorAPI.Models.Dor;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace Concentrus.DorAPI.Tests
{
    [TestClass]
    public class BaseAPITests
    {
        [TestMethod]
        public void TestMethod1()
        {
            //var jsonstr = JsonConvert.SerializeObject(new { data = new int[0], meta = new int[0] });
            //const string json = @"{'data':{},'meta':{}}";
            const string json = "{\"data\":{},\"meta\":{}}";

            var baseUri = "http://api.getdor.com/v1/";
            var endPoints = "teams";
            var accessToken =
                "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpZCI6MTIwMSwibHZsIjoiYXBpIiwiaWF0IjoxNDgzMDU3NjQ5LCJleHAiOjE0ODM2NjI0NDl9.DFwwAiwRMfYHLERyljwmXJHS4sSxzR3-CLbrEHS7PzF6CDrfrSaW3VkKQ3eKwcuWY6NRrBAWSqao58nkpCGWT_38exIxE-385q7PALewCPh5IqDm9i9bAby0bW-gW-Cv6ddLPvNQZFK7hZFy8gJ6cJxWI3qvY0qICFzzqaN8mgLVnvCKXJEonqScg62rmKWhE_bJpaqVwWfLPbFL0o_eN0TLzHNf06OG3it7YdOZqYnRiV_qPmSHwJIdRk17d3IHe2FJFKdZHpcfHgKb8DnPRlg3ziKQnWDpXOQchZ81micN5pN94tx4Hi2rEvb3WEm-nFSkE1QVJwXvRSODN0fs7g";

            var messageHandler = FakeHttpMessageHandler.GetHttpMessageHandler(
                json, HttpStatusCode.OK);

            var result = new ResponseViewModel();

            using (var httpClient = new HttpClient(messageHandler))
            {
                httpClient.BaseAddress = new Uri(baseUri);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application /octet-stream")); 

                HttpResponseMessage response = httpClient.GetAsync(endPoints).Result;

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<ResponseViewModel>().Result;
                }

                result.IsSuccessStatusCode = response.IsSuccessStatusCode;
            }

        }
    }

    public class FakeHttpMessageHandler : HttpMessageHandler
    {
        private HttpResponseMessage _response;

        public static HttpMessageHandler GetHttpMessageHandler(string content, HttpStatusCode httpStatusCode)
        {
            var memStream = new MemoryStream();

            var sw = new StreamWriter(memStream);
            sw.Write(content);
            sw.Flush();
            memStream.Position = 0;

            var httpContent = new StreamContent(memStream);

            var response = new HttpResponseMessage()
            {
                StatusCode = httpStatusCode,
                Content = httpContent
            };

            var messageHandler = new FakeHttpMessageHandler(response);

            return messageHandler;
        }

        public FakeHttpMessageHandler(HttpResponseMessage response)
        {
            _response = response;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var tcs = new TaskCompletionSource<HttpResponseMessage>();

            tcs.SetResult(_response);

            return tcs.Task;
        }
    }
}
