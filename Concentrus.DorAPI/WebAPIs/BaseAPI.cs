﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Concentrus.DorAPI.Models.Dor;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;

namespace Concentrus.DorAPI.WebAPIs
{
    public class BaseAPI: IBaseAPI
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();
        private const string BaseUri = @"http://api.getdor.com/";

        public ResponseViewModel GetToken()
        {
            Logger.Debug("CallApi | baseUri: {0}", BaseUri);

            var endPoints = $"{BaseUri}{@"v1/"}{"tokens"}";

            var apikey = ConfigurationManager.AppSettings.Get("apikey");
            Logger.Debug("GetToken | apikey: {0}", apikey);

            var refreshToken = new JObject(new JProperty("refresh_token", apikey));
            Logger.Debug("GetToken | refreshToken: {0}", refreshToken);

            var content = new StringContent(refreshToken.ToString(), Encoding.UTF8, "application/json");
            Logger.Debug("GetToken | content: {0}", JsonConvert.SerializeObject(content));

            var result = new ResponseViewModel();

            using (var client = new HttpClient())
            {
                HttpResponseMessage response = client.PostAsync(endPoints, content).Result;
                Logger.Debug("GetToken | Raw response: {0}", JsonConvert.SerializeObject(response));

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<ResponseViewModel>().Result;
                    Logger.Debug("GetToken | ResponseViewModel result: {0}", JsonConvert.SerializeObject(result));
                }
                else
                {
                    Logger.Error("GetToken | Response Status Code: {0} | Message: {1}", ((int)response.StatusCode), response.ReasonPhrase);
                }
            }

            return result;
        }

        public ResponseViewModel CallApi(string uri)
        {
            var accessToken = this.GetAccessToken();
            var endPoints = $"{@"v1/"}{uri}";
            var fullUri = $"{BaseUri}{endPoints}";

            Logger.Debug("CallApi | baseUri: {0} | accessToken: {1}", BaseUri, accessToken);
            Logger.Debug("CallApi | accessToken: {0}", accessToken);
            Logger.Debug("CallApi | endPoints:   {0}", endPoints);
            Logger.Debug("CallApi | fullUri:     {0}", fullUri);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUri);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.GetAsync(endPoints).Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Error("CallApi | Response Status Code: {0} | Message: {1}", ((int)response.StatusCode), response.ReasonPhrase);
                    return new ResponseViewModel();
                }

                var result = response.Content.ReadAsAsync<ResponseViewModel>().Result;
                result.IsSuccessStatusCode = response.IsSuccessStatusCode;

                Logger.Debug("CallApi | fullUri: {0} | response: {1}", fullUri, JsonConvert.SerializeObject(response));
                Logger.Debug("CallApi | ResponseViewModel result: {0}", JsonConvert.SerializeObject(result));

                return result;
            }
        }

        private string GetAccessToken()
        {
            var response = this.GetToken();

            var tokenViewModel = ((JObject) response.data).ToObject<TokenViewModel>();
            Logger.Debug("GetAccessToken | tokenViewModel: {0}", JsonConvert.SerializeObject(tokenViewModel));

            return tokenViewModel.Token;
        }
    }
}