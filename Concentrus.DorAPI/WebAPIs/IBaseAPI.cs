﻿using Concentrus.DorAPI.Models.Dor;

namespace Concentrus.DorAPI.WebAPIs
{
    public interface IBaseAPI
    {
        ResponseViewModel GetToken();

        ResponseViewModel CallApi(string uri);
    }
}
