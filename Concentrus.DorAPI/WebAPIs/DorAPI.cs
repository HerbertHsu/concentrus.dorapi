﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.WebPages;
using Concentrus.DorAPI.Models.Dor;
using DataLayer.Models.Dor;
using DataLayer.Services.Interface;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;

namespace Concentrus.DorAPI.WebAPIs
{
    public class DorApi : IDorAPI
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();
        private readonly IBaseAPI _baseApi;
        private readonly ITeamService _teamService;
        private readonly IStoreService _storeService;
        private readonly IFootTrafficDataService _footTrafficDataService;
        private readonly IDoorCounterService _doorCounterService;

        public DorApi(IBaseAPI baseApi, ITeamService teamService, IStoreService storeService, IFootTrafficDataService footTrafficDataService, 
            IDoorCounterService doorCounterService)
        {
            _baseApi = baseApi;
            _teamService = teamService;
            _storeService = storeService;
            _footTrafficDataService = footTrafficDataService;
            _doorCounterService = doorCounterService;
        }

        public List<TeamModel> GetTeams()
        {
            string getTeamsUri = "teams";
            var response = _baseApi.CallApi(getTeamsUri);

            Logger.Debug("GetTeams | Response Model | IsSuccessStatusCode: {0}", response.IsSuccessStatusCode);
            if (!response.IsSuccessStatusCode) return null;

            try
            {
                Logger.Debug("GetTeams | Response Model | data: {0}", response.data);
                Logger.Debug("GetTeams | Response Model | meta: {0}", response.meta);

                var teams = ((JArray) response.data).ToObject<List<TeamModel>>();

                Logger.Debug("GetTeams | teams: {0}", JsonConvert.SerializeObject(teams));

                foreach (var team in teams)
                {
                    _teamService.Add(team);
                }

                return teams;
            }
            catch (Exception e)
            {
                Logger.Error("GetTeams | Exception: {0}", e.Message);
                return null;
            }

            #region json
            //var fileName = "teams.json";
            //var jsonObject = GetJsonFile(fileName);

            //Logger.Debug("GetTeams | Json Object: {0}", jsonObject);

            //var response = jsonObject.ToObject<ResponseViewModel>();

            //Logger.Debug("GetTeams | Response Model | data: {0}", response.data);
            //Logger.Debug("GetTeams | Response Model | meta: {0}", response.meta);

            //return ((JArray)response.data).ToObject<List<TeamViewModel>>();
            #endregion
        }

        public List<StoreModel> GetStores(int? teamId)
        {
            Logger.Debug("GetStores | teamId: {0}", teamId);

            Logger.Debug("GetStores | teamId.HasValue: false");
            if (!teamId.HasValue) return null;

            string getStoresUri = $"{@"teams/"}{teamId}{@"/stores"}";
            var response = _baseApi.CallApi(getStoresUri);

            Logger.Debug("GetStores | Response Model | IsSuccessStatusCode: {0}", response.IsSuccessStatusCode);
            if (!response.IsSuccessStatusCode) return null;

            try
            {
                Logger.Debug("GetStores | Response Model | data: {0}", response.data);
                Logger.Debug("GetStores | Response Model | meta: {0}", response.meta);

                var stores = ((JArray)response.data).ToObject<List<StoreModel>>();

                Logger.Debug("GetTeams | stores: {0}", JsonConvert.SerializeObject(stores));

                foreach (var storeModel in stores)
                {
                    _storeService.Add(storeModel);
                }

                return stores;
            }
            catch (Exception e)
            {
                Logger.Debug("GetStores | Exception: {0}", e.Message);
                return null;
            }
        }

        public IEnumerable<FootTrafficDataModel> GetFootTrafficEventByDate(int teamId, int storeId, string date)
        {
            if (date.IsNullOrWhiteSpace() && date.IsEmpty())
            {
                return null;
            }

            string getFTEByDateUri = $"{@"teams/"}{teamId}{@"/stores/"}{storeId}{@"/days/"}{date}";
            Logger.Debug("GetFootTrafficEventByDate | getFTEByDateUri: {0}", getFTEByDateUri);

            var response = _baseApi.CallApi(getFTEByDateUri);

            Logger.Debug("GetFootTrafficEventByDate | Response Model | IsSuccessStatusCode: {0}", response.IsSuccessStatusCode);
            if (!response.IsSuccessStatusCode) return new List<FootTrafficDataModel>();

            try
            {
                Logger.Debug("GetFootTrafficEventByDate | Response Model | data: {0}", response.data);
                Logger.Debug("GetFootTrafficEventByDate | Response Model | meta: {0}", response.meta);

                var ftdataViewModel = ((JObject)response.data).ToObject<FootTrafficDataViewModel>();
                Logger.Debug("GetFootTrafficEventByDate | ftdataViewModel: {0}", JsonConvert.SerializeObject(ftdataViewModel));

                var ftdataModel = ftdataViewModel.ToFootTrafficDataModel();
                Logger.Debug("GetFootTrafficEventByDate | ftdataModel: {0}", JsonConvert.SerializeObject(ftdataModel));

                var models = ftdataModel as IList<FootTrafficDataModel> ?? ftdataModel.ToList();
                Logger.Debug("GetFootTrafficEventByDate | models: {0}", JsonConvert.SerializeObject(models));

                foreach (var footTrafficDataModel in models)
                {
                    _footTrafficDataService.Add(footTrafficDataModel);
                }

                return models;
            }
            catch (Exception e)
            {
                Logger.Debug("GetFootTrafficEventByDate | Exception: {0}", e.Message);
                return null;
            }
        }

        public ResponseViewModel GetAccessToken()
        {
            return _baseApi.GetToken();
        }

        public IEnumerable<FootTrafficDataModel> ProcessDorCounter(string dateFromString, string dateToString)
        {
            try
            {
                var teams = this.GetTeams();
                Logger.Debug("ProcessDorCounter | teams: {0}", JsonConvert.SerializeObject(teams));

                if (teams == null) return null;

                var storeList = GetStoreList(teams);
                Logger.Debug("ProcessDorCounter | storeList: {0}", JsonConvert.SerializeObject(storeList));

                var dateRange = GetAllDates(dateFromString, dateToString);
                Logger.Debug("ProcessDorCounter | dateRange: {0}", JsonConvert.SerializeObject(dateRange));

                return GetFootTrafficDataList(storeList, dateRange);
            }
            catch (Exception e)
            {
                Logger.Error("ProcessDorCounter | Exception: {0}", e.Message);
                return null;
            }
        }

        private IEnumerable<StoreModel> GetStoreList(IEnumerable<TeamModel> teams)
        {
            var storeList = new List<StoreModel>();

            foreach (var teamModel in teams)
            {
                var stores = this.GetStores(teamModel.Id);
                storeList.AddRange(stores);
            }

            return storeList;
        } 

        private IEnumerable<FootTrafficDataModel> GetFootTrafficDataList(IEnumerable<StoreModel> stores, IEnumerable<DateTime> dateRange)
        {
            var ftdataList = new List<FootTrafficDataModel>();
            var storeModels = stores as IList<StoreModel> ?? stores.ToList();

            foreach (var date in dateRange)
            {
                foreach (var storeModel in storeModels)
                {
                    var ftdata = this.GetFootTrafficEventByDate(storeModel.Team_Id, storeModel.Id, date.ToString("yyyy-MM-dd")).ToList();
                    Logger.Debug("GetFootTrafficDataList | ftdata: {0}", JsonConvert.SerializeObject(ftdata));

                    if (ftdata.Count == 0) continue;

                    _doorCounterService.AddBulk(ftdata);
                    Logger.Trace("GetFootTrafficDataList | _doorCounterService.AddBulk Done");

                    ftdataList.AddRange(ftdata);
                }
            }

            return ftdataList;
        }

        private IEnumerable<DateTime> GetAllDates(string dateFrom, string dateTo)
        {
            Logger.Debug("GetAllDates | dateFrom: {0} | dateTo: {1}", dateFrom, dateTo);

            var dateFromDateTimeFormat = DateTime.ParseExact(dateFrom, "yyyy-MM-dd", null);
            var dateToDateTimeFormat = DateTime.ParseExact(dateTo, "yyyy-MM-dd", null);

            var isDateFromEqualsDateTo = DateTime.Equals(dateFromDateTimeFormat, dateToDateTimeFormat);

            var dates = new List<DateTime>() { dateFromDateTimeFormat };

            if (!isDateFromEqualsDateTo)
            {
                dates.Add(dateToDateTimeFormat);
            }

            try
            {
                Logger.Debug("GetAllDates | dates: {0}", JsonConvert.SerializeObject(dates));
                if (dates.Count == 1) return dates;

                var days = (dates.Max() - dates.Min()).Days;

                var otherDays = Enumerable.Range(0, days)
                        .Select(d => dates.Min().AddDays(d))
                        .Except(dates);

                dates.AddRange(otherDays);
                dates.Sort();
            }
            catch (Exception ex)
            {
                Logger.Error("GetAllDates | Error Message: {0}", ex.Message);
            }

            return dates;
        }

        //TODO
        public ResponseViewModel GetFootTrafficEventByDays(string startDate, string endDate)
        {
            //string getFTEByDaysUri = "";

            if (!startDate.IsNullOrWhiteSpace() || startDate != string.Empty)
            {
                //getFTEByDaysUri = "";

                if (!endDate.IsNullOrWhiteSpace() || endDate != string.Empty)
                {
                    //getFTEByDaysUri = "";
                }
            }

            //var result = CallApi(getStoresUri);
            //return result;

            return new ResponseViewModel();
        }

        #region GetJsonFile
        //private JObject GetJsonFile(string fileName)
        //{
        //    string fullPathFileName = $"{@"D:\IIS\DorAPI\Json\"}{fileName}";

        //    Logger.Info("GetJsonFile | FullPathFileName: {0}", fullPathFileName);

        //    JObject responseJObject = new JObject();

        //    using (StreamReader file = File.OpenText(fullPathFileName))
        //    {
        //        using (JsonTextReader reader = new JsonTextReader(file))
        //        {
        //            responseJObject = (JObject)JToken.ReadFrom(reader);
        //        }
        //    }

        //    return responseJObject;
        //}
        #endregion
    }
}