﻿using System.Collections.Generic;
using Concentrus.DorAPI.Models.Dor;
using DataLayer.Models.Dor;

namespace Concentrus.DorAPI.WebAPIs
{
    public interface IDorAPI
    {
        List<TeamModel> GetTeams();

        List<StoreModel> GetStores(int? teamId);

        IEnumerable<FootTrafficDataModel> GetFootTrafficEventByDate(int teamId, int storeId, string date);

        ResponseViewModel GetAccessToken();

        IEnumerable<FootTrafficDataModel> ProcessDorCounter(string dateFromString, string dateToString);

        //ResponseViewModel GetFootTrafficEventByDays(string startDate, string endDate);
    }
}