﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Concentrus.DorAPI.WebAPIs;
using DataLayer.Repository.Implementation;
using DataLayer.Repository.Interface;
using DataLayer.Services.Implementation;
using DataLayer.Services.Interface;

namespace Concentrus.DorAPI.App_Start
{
    public static class AutofacConfig
    {
        public static void Bootstrapper()
        {
            // Get configurations

            // Register your Web API controllers.
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            //WebAPIs
            builder.RegisterType<WebAPIs.DorApi>().As<IDorAPI>();
            builder.RegisterType<WebAPIs.BaseAPI>().As<IBaseAPI>();

            //Repository
            builder.RegisterType<AddressRepository>().As<IAddressRepository>();
            builder.RegisterType<CustomerRepository>().As<ICustomerRepository>();
            builder.RegisterType<DeviceRepository>().As<IDeviceRepository>();
            builder.RegisterType<DevicesRepository>().As<IDevicesRepository>();
            builder.RegisterType<DoorCounterRepository>().As<IDoorCounterRepository>();
            builder.RegisterType<FootTrafficDataRepository>().As<IFootTrafficDataRepository>();
            builder.RegisterType<StoreRepository>().As<IStoreRepository>();
            builder.RegisterType<TeamRepository>().As<ITeamRepository>();

            //Services
            builder.RegisterType<DoorCounterService>().As<IDoorCounterService>();
            builder.RegisterType<FootTrafficDataService>().As<IFootTrafficDataService>();
            builder.RegisterType<StoreService>().As<IStoreService>();
            builder.RegisterType<TeamService>().As<ITeamService>();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}