﻿using System;
using System.Collections.Generic;
using AutoMapper;
using DataLayer.Models.Dor;

namespace Concentrus.DorAPI.Models.Dor
{
    public class FootTrafficDataViewModel
    {
        public int Store_Id { get; set; }

        public string Date { get; set; }

        public int In_Count { get; set; }

        public int Out_Count { get; set; }

        public IEnumerable<HourViewModel> Hours { get; set; }

        public IEnumerable<FootTrafficDataModel> ToFootTrafficDataModel()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<HourViewModel, FootTrafficDataModel>()
                .ForMember(dest => dest.Store_Id, opt => opt.MapFrom(src => src.Store_Id))
                .ForMember(dest => dest.Date, opt => opt.MapFrom(src => Convert.ToDateTime(src.Date)))
                .ForMember(dest => dest.Datetime, opt => opt.MapFrom(src => src.Datetime))
                .ForMember(dest => dest.Hour, opt => opt.MapFrom(src => ParseHour(src.Datetime)))
                .ForMember(dest => dest.In_Count, opt => opt.MapFrom(src => src.In_Count))
                .ForMember(dest => dest.Out_Count, opt => opt.MapFrom(src => src.Out_Count)));

            return config.CreateMapper().Map<IEnumerable<FootTrafficDataModel>>(this.Hours);
        }

        private string ParseHour(string datetime)
        {
            return datetime.Substring(datetime.Length - 2, 2);
        }
    }
}