﻿namespace Concentrus.DorAPI.Models.Dor
{
    public class TokenViewModel
    {
        public string Token { get; set; }

        public string Refresh_Token { get; set; }

        public int User_Id { get; set; }

        public string Level { get; set; }
    }
}