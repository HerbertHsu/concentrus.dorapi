﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Concentrus.DorAPI.Models.Dor
{
    public class HourViewModel
    {
        public int Store_Id { get; set; }

        public string Datetime { get; set; }

        public string Date { get; set; }

        public int In_Count { get; set; }

        public int Out_Count { get; set; }
    }
}