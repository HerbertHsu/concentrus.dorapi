﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Concentrus.DorAPI.Models.Dor
{
    public class ResponseViewModel
    {
        public bool IsSuccessStatusCode { get; set; }

        public dynamic data { get; set; }

        public dynamic meta { get; set; }

        public JObject ToJObject()
        {
            return JObject.FromObject(new
            {
                IsSuccessStatusCode,
                data,
                meta
            });
        }

        protected bool Equals(ResponseViewModel other)
        {
            return JsonConvert.SerializeObject(this) == JsonConvert.SerializeObject(other);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ResponseViewModel) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((data != null ? data.GetHashCode() : 0)*397) ^ (meta != null ? meta.GetHashCode() : 0);
            }
        }
    }
}