﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Concentrus.DorAPI.Models.DoorCounter
{
    public class DoorCounterViewModel
    {
        public int Id { get; set; }

        public int SiteId { get; set; }

        public string CustomerCID { get; set; }

        public string CustomerName { get; set; }

        public DateTime Date { get; set; }

        public int Hour { get; set; }

        public int Count { get; set; }

        public string DoorDateKey { get; set; }

        public string VendorName { get; set; }
    }
}