﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Concentrus.DorAPI.Models.Dor;
using Concentrus.DorAPI.WebAPIs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;

namespace Concentrus.DorAPI.Controllers
{
    [RoutePrefix("Dor")]
    public class DorController : ApiController
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();
        private readonly IDorAPI _dorApi;

        public DorController(IDorAPI dorApi)
        {
            _dorApi = dorApi;
        }

        [Route("GetAccessToken")]
        [HttpGet]
        public IHttpActionResult GetAccesToken()
        {
            try
            {
                var tokenData = _dorApi.GetAccessToken();
                Logger.Debug("DorController | GetAccesToken | tokenData: {0}", JsonConvert.SerializeObject(tokenData));

                if (tokenData == null) return NotFound();

                return Ok(JObject.FromObject(tokenData));
            }
            catch (Exception ex)
            {
                Logger.Error("Exception Message: {0}", ex.Message);
                return BadRequest();
            }
        }

        [Route("GetTeams")]
        [HttpGet]
        public IHttpActionResult GetTeams()
        {
            try
            {
                var teams = _dorApi.GetTeams();
                Logger.Debug("DorController | GetTeams | teams: {0}", JsonConvert.SerializeObject(teams));

                if (teams == null) return NotFound();

                return Ok(JArray.FromObject(teams));
            }
            catch (Exception ex)
            {
                Logger.Error("Exception Message: {0}", ex.Message);
                return BadRequest();
            }
        }

        [Route("GetStores")]
        [HttpGet]
        public IHttpActionResult GetStores(int? teamId)
        {
            try
            {
                var stores = _dorApi.GetStores(teamId);
                Logger.Debug("DorController | GetStores | stores: {0}", JsonConvert.SerializeObject(stores));

                if (stores == null) return NotFound();

                return Ok(JArray.FromObject(stores));
            }
            catch (Exception ex)
            {
                Logger.Error("Exception Message: {0}", ex.Message);
                return BadRequest();
            }
        }

        [Route("GetFootTrafficEventByDate")]
        [HttpGet]
        public IHttpActionResult GetFootTrafficEventByDate(int teamId, int storeId, string date)
        {
            try
            {
                var footTrafficData = _dorApi.GetFootTrafficEventByDate(teamId, storeId, date);
                Logger.Debug("DorController | GetFootTrafficEventByDate | footTrafficData: {0}", JsonConvert.SerializeObject(footTrafficData));

                if (footTrafficData == null) return NotFound();

                return Ok(JArray.FromObject(footTrafficData));
            }
            catch (Exception ex)
            {
                Logger.Error("Exception Message: {0}", ex.Message);
                return BadRequest();
            }
        }

        [Route("ProcessDorCounter")]
        [HttpGet]
        public IHttpActionResult ProcessDorCounter(string dateFrom, string dateTo)
        {
            try
            {
                Logger.Debug("DorController | ProcessDorCounter | dateFrom: {0}, dateTo: {1}", dateFrom, dateTo);

                var response = _dorApi.ProcessDorCounter(dateFrom, dateTo);
                Logger.Debug("DorController | ProcessDorCounter | response: {0}", JsonConvert.SerializeObject(response));

                if (response == null) return NotFound();

                return Ok(JArray.FromObject(response));
            }
            catch (Exception ex)
            {
                Logger.Error("Exception Message: {0}", ex.Message);
                return BadRequest();
            }
        }

        //[Route("GetFootTrafficEventByDays")]
        //[HttpGet]
        //public IHttpActionResult GetFootTrafficEventByDays(string startDate, string endDate)
        //{
        //    try
        //    {
        //        var footTrafficEventByDays = _dorApi.GetFootTrafficEventByDays(startDate, endDate);

        //        //return Ok(JsonConvert.SerializeObject(footTrafficEventByDays));
        //        return Ok(JArray.FromObject(footTrafficEventByDays));
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("Exception Message: {0}", ex.Message);
        //        return BadRequest();
        //    }
        //}
    }
}
