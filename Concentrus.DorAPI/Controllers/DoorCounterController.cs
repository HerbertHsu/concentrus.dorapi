﻿using System;
using System.Web.Http;
using DataLayer.Models.DoorCounter;
using DataLayer.Models.Dor;
using DataLayer.Repository.Interface;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;

namespace Concentrus.DorAPI.Controllers
{
    [RoutePrefix("DoorCounter")]
    public class DoorCounterController : ApiController
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();
        private IDoorCounterRepository _doorCounterRepo;

        public DoorCounterController(IDoorCounterRepository doorCounterRepo)
        {
            _doorCounterRepo = doorCounterRepo;
        }

        [Route("Get")]
        [HttpGet]
        public IHttpActionResult Get()
        {
            var device = new DeviceModel
            {
                Created_At = DateTime.Now.ToShortDateString(),
                Deleted_At = DateTime.Now.ToShortDateString(),
                Device_Type_Name = "Herbert",
                Serial_Number = "ass hole"
            };

            try
            {
                return Ok(device);
            }
            catch (Exception ex)
            {
                Logger.Error("Exception Message: {0}", ex.Message);
                return BadRequest();
            }
        }

        [Route("Save")]
        [HttpPost]
        public IHttpActionResult Save(JObject model)
        {
            var dcModel = new DoorCounterModel
            {
                Id = (int)model["Id"],
                SiteId = (int)model["SiteId"],
                CustomerCID = model["CustomerCID"].ToString(),
                CustomerName = model["CustomerName"].ToString(),
                Date = Convert.ToDateTime(model["Date"]),
                Hour = (int)model["Hour"],
                Count = (int)model["Count"],
                VendorName = string.Empty
            };

            try
            {
                dcModel.DoorDateKey = $"{dcModel.CustomerCID}{dcModel.Date.Year}{dcModel.Date.Month.ToString("00")}{dcModel.Date.Day.ToString("00")}{dcModel.Hour.ToString("00")}";

                Logger.Debug(JsonConvert.SerializeObject(dcModel));

                _doorCounterRepo.Add(dcModel);

                // return Json(dcModel);
                return Ok(dcModel);
            }
            catch (Exception ex)
            {
                Logger.Error("Exception Message: {0}", ex.Message);
                return BadRequest();
            }
        }
    }
}
