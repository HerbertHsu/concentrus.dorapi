﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;

namespace DorConsoleApp
{
    public class Program
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        private static void Main(string[] args)
        {
            Logger.Info("Program starts...");

            AppDomain.CurrentDomain.UnhandledException += GlobalExceptionHandler;
            
            var dorConsoleApp = new DorConsoleApp();
            
            var dateFrom = dorConsoleApp.GetDateFrom();
            var dateTo = dorConsoleApp.GetDateTo();
            Logger.Debug("Main | dateFrom: {0} | dateTo: {1}", dateFrom, dateTo);

            dorConsoleApp.CallApi(dateFrom,  dateTo);

            Logger.Info("Program finishes.");

        }

        private static void GlobalExceptionHandler(object sender, EventArgs args)
        {
            Logger.Fatal(args.ToString);
            Environment.Exit(1);
        }
    }

    public class DorConsoleApp
    {
        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();
        private readonly string _baseUri = System.Configuration.ConfigurationManager.AppSettings["baseUri"];
        private readonly string _endPoint = System.Configuration.ConfigurationManager.AppSettings["endPoint"];
        private readonly string _dateFrom = System.Configuration.ConfigurationManager.AppSettings["dateFrom"];
        private readonly string _dateTo = System.Configuration.ConfigurationManager.AppSettings["dateTo"];

        public void CallApi(string dateFrom, string dateTo)
        {
            var endPointWithParam = $"{_endPoint}{"?dateFrom="}{dateFrom}{"&dateTo="}{dateTo}";

            _logger.Debug("DorConsoleApp | baseUri: {0}", _baseUri);
            _logger.Debug("DorConsoleApp | endPoint: {0}", _endPoint);
            _logger.Debug("DorConsoleApp | endPointWithParam: {0}", endPointWithParam);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseUri);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.GetAsync(endPointWithParam).Result;
                _logger.Debug("DorConsoleApp | Raw response: {0}", JsonConvert.SerializeObject(response));

                var result = response.Content.ReadAsAsync<JArray>().Result;
                _logger.Debug("DorConsoleApp | JArray result: {0}", result);

                if (response.IsSuccessStatusCode)
                {
                    _logger.Debug("DorConsoleApp | JArray result: {0}", result);
                }
                else
                {
                    _logger.Error("DorConsoleApp | Response Status Code: {0} | Message: {1}", ((int)response.StatusCode), response.ReasonPhrase);
                }
            }
        }

        public string GetDateFrom()
        {
            return _dateFrom == string.Empty ? DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd") : _dateFrom;
        }

        public string GetDateTo()
        {
            return _dateTo == string.Empty ? DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd") : _dateTo;
        }
    }
}
